class WhyUsTranslations {
    
    GetCurrentLanguageAction;
    WhyUsTitleConcept;
    TitleText = "Eum ipsam laborum deleniti";
    WhyUsStrongTitleConcept;
    TitleTextStrong = "velit pariatur architecto aut nihil";
    WhyUsTextConcept;
    LoremText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit";
    WhyUsAccordionTitleConcept;
    AccodionTitleText = " Non consectetur a erat nam at lectus urna duis? ";
    WhyUsAccordionTextConcept;

    get WhyUsTitle()
    {
        return this.WhyUsTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get WhyUsTitleStrong()
    {
        return this.WhyUsStrongTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get WhyUsText()
    {
        return this.WhyUsTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get WhyUsAccordionTitle()
    {
        return this.WhyUsAccordionTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get WhyUsAccordionText()
    {
        return this.WhyUsAccordionTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    constructor(getCurrentLanguage){
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.WhyUsTitleConcept = new Concept(this.TitleText, this.TitleText, this.TitleText, this.TitleText, this.TitleText);
        this.WhyUsStrongTitleConcept = new Concept(this.TitleTextStrong, this.TitleTextStrong, this.TitleTextStrong, this.TitleTextStrong, this.TitleTextStrong);
        this.WhyUsTextConcept = new Concept(this.LoremText, this.LoremText, this.LoremText, this.LoremText, this.LoremText);
        this.WhyUsAccordionTitleConcept = new Concept(this.AccodionTitleText, this.AccodionTitleText, this.AccodionTitleText, this.AccodionTitleText, this.AccodionTitleText);
        this.WhyUsAccordionTextConcept = new Concept(this.LoremText, this.LoremText, this.LoremText, this.LoremText, this.LoremText);
    }
    
}
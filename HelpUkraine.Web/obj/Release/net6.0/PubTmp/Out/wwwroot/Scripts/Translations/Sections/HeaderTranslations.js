class HeaderTranslations
{
    HomeConcept;
    AboutTitleConcept;
    SelectLanguageConcept;

    GetCurrentLanguageAction;

    get HomeTitle()
    {
        return this.HomeConcept.Translate(this.GetCurrentLanguageAction());
    }

    get AboutTitle()
    {
        return this.AboutTitleConcept.Translate(this.GetCurrentLanguageAction());
    }

    get SelectLanguageTitle()
    {
        //return "a";
        return this.SelectLanguageConcept.Translate(this.GetCurrentLanguageAction());
    }

    constructor(getCurrentLanguage)
    {
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.HomeConcept = new Concept("Home", "Home", "Додому", "Дома", "Home");Concept;
        this.AboutTitleConcept = new Concept("About", "About", "Про", "О", "About");
        this.SelectLanguageConcept = new Concept("Select Language", "Select Language", "Оберіть мову", "Выбрать язык", "Select Language");
    }
}
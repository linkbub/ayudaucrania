class ServicesTranslations
{
    GetCurrentLanguageAction;
    ServicesTitleConcept;
    ServicesTextConcept;
    LoremText = "Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas."
    ServiceCardLinkConcept;
    LoremCardLink = "Lorem Ipsum";

    get ServicesTitle(){
        return this.ServicesTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesText(){
        return this.ServicesTextConcept.Translate(this.GetCurrentLanguageAction());
    }

    constructor (getCurrentLanguage)
    {   
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.ServicesTitleConcept = new Concept("Servicios", "Services", "послуги", "Услуги", "Serveis");
        this.ServicesTextConcept = new Concept(this.LoremText, this.LoremText, this.LoremText, this.LoremText, this.LoremText);
        this.ServiceCardLinkConcept = new Concept(this.LoremCardLink, this.LoremCardLink, this.LoremCardLink, this.LoremCardLink, this.LoremCardLink);
    }
}
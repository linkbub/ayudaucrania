class IconBoxTranslations
{
    IconBoxConcept;
    GetCurrentLanguageAction;
    LoremIpsum = "Lorem Ipsum";
    LoremIpsum2 = "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident";

    get IconBoxTitleOne()
    {
        return this.IconBoxTitleConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTitleTwo()
    {
        return this.IconBoxTitleConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTitleThree()
    {
        return this.IconBoxTitleConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTitleFour()
    {
        return this.IconBoxTitleConceptFour.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextOne()
    {
        return this.IconBoxTextConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextTwo()
    {
        return this.IconBoxTextConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextThree()
    {
        return this.IconBoxTextConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextFour()
    {
        return this.IconBoxTextConceptFour.Translate(this.GetCurrentLanguageAction());
    }


    constructor(getCurrentLanguage)
    {
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.IconBoxTitleConceptOne = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTitleConceptTwo = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTitleConceptThree = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTitleConceptFour = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTextConceptOne = new Concept(this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2);
        this.IconBoxTextConceptTwo = new Concept(this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2);
        this.IconBoxTextConceptThree = new Concept(this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2);
        this.IconBoxTextConceptFour = new Concept(this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2, this.LoremIpsum2);
    }
}
class AboutTranslations
{
    AboutTitleConcept;
    AboutTextConcept;
    AboutListElementConcept;
    AboutButtonTextConcept;
    GetCurrentLanguageAction;
    LoremIpsum="Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.";
    LoremIpsumList="Ullamco laboris nisi ut aliquip ex ea commodo consequat";

    get AboutTitle()
    {
        return this.AboutTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutTextOne()
    {
        return this.AboutTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutTextTwo()
    {
        return this.AboutTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutTextThree()
    {
        return this.AboutTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutListTextOne()
    {
        return this.AboutListElementConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutListTextTwo()
    {
        return this.AboutListElementConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutListTextThree()
    {
        return this.AboutListElementConcept.Translate(this.GetCurrentLanguageAction());
    }
    get ButtonText()
    {
        return this.AboutButtonTextConcept.Translate(this.GetCurrentLanguageAction());
    }

    constructor(getCurrentLanguage)
    {
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.AboutTitleConcept = new Concept("SOBRE NOSOTROS", "ABOUT US", "про нас", "о нас", "Home");
        this.AboutTextConcept = new Concept(this.LoremIpsum,this.LoremIpsum,this.LoremIpsum,this.LoremIpsum,this.LoremIpsum);
        this.AboutListElementConcept = new Concept(this.LoremIpsumList,this.LoremIpsumList,this.LoremIpsumList,this.LoremIpsumList,this.LoremIpsumList,);
        this.AboutButtonTextConcept = new Concept("Saber más", "Learn more", "Дізнайтеся більше", "Узнать больше", "Més informació");
    }
}
class Landing
{
    _currentLanguage = 3;
    get CurrentLanguage()
    {
        return this._currentLanguage;
    }
    set CurrentLanguage(value)
    {
        this._currentLanguage = value;
    }

    get CurrentFlag()
    {
        switch(this.CurrentLanguage)
        {
            default:
            case LanguagesTypes.Es:
                return "./assets/img/flag_kit/espana.png";
            case LanguagesTypes.En:
                return "./assets/img/flag_kit/reino-unido.png";
            case LanguagesTypes.Ua:
                return "./assets/img/flag_kit/ucrania.png";
            case LanguagesTypes.Cat:
                return "./assets/img/flag_kit/catalan.png";
            case LanguagesTypes.Ru:
                return "./assets/img/flag_kit/rusia.png";
        }
    }

    HeaderTranslations;
    AboutTranslastions;
    CarouselTranslastions;
    IconBoxTranslations;
    WhyUsTranslations;

    get Test()
    {
        return 10;
    }

    get IsLogon()
    {
        return App.ClientGlobals.IsLogon;
    }

    constructor()
    {
        this.CurrentLanguage = LanguagesTypes.Ua;
        this.HeaderTranslations = new HeaderTranslations(()=>this.CurrentLanguage);
        this.AboutTranslations = new AboutTranslations(()=>this.CurrentLanguage);
        this.CarouselTranslations = new CarouselTranslations(()=>this.CurrentLanguage);
        this.IconBoxTranslations = new IconBoxTranslations(()=>this.CurrentLanguage);
        this.WhyUsTranslations = new WhyUsTranslations(()=>this.CurrentLanguage);
        this.ServicesTranslations = new ServicesTranslations(()=>this.CurrentLanguage);
    }

    SelectLanguage(languageType)
    {
        this.CurrentLanguage = languageType;
    }
}

App.controller('LandingViewModel', Landing);
class CarouselTranslations
{
    CarouselTitleConcept;
    CarouselTextConcept;
    GetCurrentLanguageAction;
    LoremIpsum = "Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.";

    get CarouselTitleOne()
    {
        return this.CarouselTitleConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTitleTwo()
    {
        return this.CarouselTitleConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTitleThree()
    {
        return this.CarouselTitleConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextOne()
    {
        return this.CarouselTextConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextTwo()
    {
        return this.CarouselTextConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextThree()
    {
        return this.CarouselTextConceptOne.Translate(this.GetCurrentLanguageAction());
    }

    get CarouselButtonText()
    {
        return this.CarouselButtonTextConcept.Translate(this.GetCurrentLanguageAction());
    }

    constructor(getCurrentLanguage)
    {

        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.CarouselTitleConceptOne = new Concept("Bienvenido a", "Wellcome", "ласкаво просимо до", "Добро пожаловать на", "Benvigut a");
        this.CarouselTextConceptOne = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.CarouselTitleConceptTwo = new Concept("Bienvenido a", "Wellcome", "ласкаво просимо до", "Добро пожаловать на", "Benvigut a");
        this.CarouselTextConceptTwo = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.CarouselTitleConceptThree = new Concept("Bienvenido a", "Wellcome", "ласкаво просимо до", "Добро пожаловать на", "Benvigut a");
        this.CarouselTextConceptThree = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.CarouselButtonTextConcept = new Concept("Leer más", "Read more", "Детальніше", "Подробнее", "Llegir més");
    }
}
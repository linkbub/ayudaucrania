var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseDefaultFiles();
app.UseStaticFiles();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

app.UseEndpoints(endpoints =>
{
    //endpoints.MapGet(".well-known/acme-challenge/{id}", async (context) =>
    //{
    //    var id = context.Request.RouteValues["id"].ToString();
    //    var file = System.IO.Path.Combine("env.WebRootPath", ".well-known", "acme-challenge", id);
    //    await context.Response.SendFileAsync(file);
    //});
    //endpoints.MapControllers();
}).UseSpa(_ => { });

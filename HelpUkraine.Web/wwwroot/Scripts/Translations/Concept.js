class Concept
{
    Spanish = "";
    English = "";
    Ukraine = "";
    Russian = "";
    Catalonian = "";

    constructor(spanish, english, ukraine, russian, catalonian)    
    {
        this.Spanish = spanish;
        this.English = english;
        this.Ukraine = ukraine;
        this.Russian = russian;
        this.Catalonian = catalonian;
    }

    Translate(languageType)
    {
        switch(languageType)
        {
            case LanguagesTypes.Es:
                return this.Spanish;
            case LanguagesTypes.En:
                return this.English;
            case LanguagesTypes.Ru:
                return this.Russian;
            case LanguagesTypes.Cat:
                return this.Catalonian;
            default:
            case LanguagesTypes.Ua:
                return this.Ukraine;

        }
    }

}


LanguagesTypes =
{
    Es : 1,
    En : 2,
    Ua : 3,
    Ru : 4,
    Cat : 5
}
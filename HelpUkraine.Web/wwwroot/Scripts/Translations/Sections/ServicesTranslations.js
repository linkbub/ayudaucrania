class ServicesTranslations
{
    GetCurrentLanguageAction;
    ServicesTitleConcept;
    ServicesTextConceptOne;
    ServicesTextConceptTwo;
    ServicesTextConceptThree;
    ServicesTextConceptFour;
    ServicesTextConceptFive;
    ServicesTextConceptSix;
    LoremText = "Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas."
    ServiceCardLinkConcept;
    LoremCardLink = "Lorem Ipsum";

    get ServicesTitle(){
        return this.ServicesTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesText(){
        return this.ServicesTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesCardOne(){
        return this.ServiceCardLinkConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesCardTwo(){
        return this.ServiceCardLinkConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesCardThree(){
        return this.ServiceCardLinkConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesCardFour(){
        return this.ServiceCardLinkConceptFour.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesCardFive(){
        return this.ServiceCardLinkConceptFive.Translate(this.GetCurrentLanguageAction());
    }
    get ServicesCardSix(){
        return this.ServiceCardLinkConceptSix.Translate(this.GetCurrentLanguageAction());
    }

    constructor (getCurrentLanguage)
    {   
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.ServicesTitleConcept = new Concept("Servicios", "Services", "послуги", "Услуги", "Serveis");
        this.ServicesTextConcept = new Concept(
            "Gracias a las aportaciones de nuestros asociados podemos ofrecer actividades dirigidasa la ayuda, formación y visualización de la igualdad de derechos.", 
            "Gracias a las aportaciones de nuestros asociados podemos ofrecer actividades dirigidasa la ayuda, formación y visualización de la igualdad de derechos.", 
            "Gracias a las aportaciones de nuestros asociados podemos ofrecer actividades dirigidasa la ayuda, formación y visualización de la igualdad de derechos.", 
            "Gracias a las aportaciones de nuestros asociados podemos ofrecer actividades dirigidasa la ayuda, formación y visualización de la igualdad de derechos.", 
            "Gràcies a les aportacions de les persones associades podem oferir activitats d’ajuda, formació i visualització de la igualtat de drets."
            );
        this.ServiceCardLinkConceptOne = new Concept(
            "Formación y charlas sobre la situación y nuestro procedimiento de actuación", 
            "Formación y charlas sobre la situación y nuestro procedimiento de actuación", 
            "Formación y charlas sobre la situación y nuestro procedimiento de actuación",
            "Formación y charlas sobre la situación y nuestro procedimiento de actuación", 
            "Formació i xerrades sobre la situació i precediment de actuació"
            );
        this.ServiceCardLinkConceptTwo = new Concept(
            "Coloquios y cenas debates con mujeres de éxito", 
            "Coloquios y cenas debates con mujeres de éxito", 
            "Coloquios y cenas debates con mujeres de éxito",
            "Coloquios y cenas debates con mujeres de éxito", 
            "Col·loquis i sopars debat amb dones d’èxit."
            );
        this.ServiceCardLinkConceptThree = new Concept(
            "Ayuda en acción con los proyectos SALVAVIDES. Actualmente con la guerra de Ucrania.", 
            "Ayuda en acción con los proyectos SALVAVIDES. Actualmente con la guerra de Ucrania.", 
            "Ayuda en acción con los proyectos SALVAVIDES. Actualmente con la guerra de Ucrania.",
            "Ayuda en acción con los proyectos SALVAVIDES. Actualmente con la guerra de Ucrania.", 
            "Ajuda en acció amb els projectes SALVAVIDES. Actualment amb la guerra d’Ucraïna."
            ); 
        this.ServiceCardLinkConceptFour = new Concept(
            "Eventos y actividades que promocionan la equidad e igualdad.", 
            "Eventos y actividades que promocionan la equidad e igualdad.", 
            "Eventos y actividades que promocionan la equidad e igualdad.",
            "Eventos y actividades que promocionan la equidad e igualdad.", 
            "Esdeveniments i activitats que promocionen l’equitat i la igualtat."
            );
        this.ServiceCardLinkConceptFive = new Concept(
            "Asesoría jurídica.", 
            "Asesoría jurídica.", 
            "Asesoría jurídica.",
            "Asesoría jurídica.", 
            "Assessoria jurídica."
            );
        this.ServiceCardLinkConceptSix = new Concept(
            "Lucha contra cualquier tipo de violencia.", 
            "Lucha contra cualquier tipo de violencia.", 
            "Lucha contra cualquier tipo de violencia.",
            "Lucha contra cualquier tipo de violencia.", 
            "Lluita contra qualsevol tipus de violència."
            );
    }
}
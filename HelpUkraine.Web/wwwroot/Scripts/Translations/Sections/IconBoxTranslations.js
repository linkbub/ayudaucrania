class IconBoxTranslations
{
    IconBoxConcept;
    GetCurrentLanguageAction;
    LoremIpsum = "Lorem Ipsum";
    LoremIpsum2 = "Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident";

    get IconBoxTitleOne()
    {
        return this.IconBoxTitleConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTitleTwo()
    {
        return this.IconBoxTitleConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTitleThree()
    {
        return this.IconBoxTitleConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTitleFour()
    {
        return this.IconBoxTitleConceptFour.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextOne()
    {
        return this.IconBoxTextConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextTwo()
    {
        return this.IconBoxTextConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextThree()
    {
        return this.IconBoxTextConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get IconBoxTextFour()
    {
        return this.IconBoxTextConceptFour.Translate(this.GetCurrentLanguageAction());
    }


    constructor(getCurrentLanguage)
    {
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.IconBoxTitleConceptOne = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTitleConceptTwo = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTitleConceptThree = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTitleConceptFour = new Concept(this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum, this.LoremIpsum);
        this.IconBoxTextConceptOne = new Concept(
            "Campaña Salvavides, Objetivo Ucránia. Estamos ayudando a las personas que huyen de la guerra de Ucrania facilitándoles ayudas como ser: Alojamiento, ropa, alimentos y medicina.", 
            "Campaña Salvavides, Objetivo Ucránia. Estamos ayudando a las personas que huyen de la guerra de Ucrania facilitándoles ayudas como ser: Alojamiento, ropa, alimentos y medicina.", 
            "Campaña Salvavides, Objetivo Ucránia. Estamos ayudando a las personas que huyen de la guerra de Ucrania facilitándoles ayudas como ser: Alojamiento, ropa, alimentos y medicina.", 
            "Campaña Salvavides, Objetivo Ucránia. Estamos ayudando a las personas que huyen de la guerra de Ucrania facilitándoles ayudas como ser: Alojamiento, ropa, alimentos y medicina.", 
            "Campanya SALAVIDES. OBJECTIU UCRAÏNA. Estem ajudant les persones que fugen de la guerra d’Ucraïna, aportant allotjament, roba, menjar o medecines."
            );
        this.IconBoxTextConceptTwo = new Concept(
            "Contamos con transportes para el envío de material humanitario a Ucrania y facilitar la llegada de refugiados a España.", 
            "Contamos con transportes para el envío de material humanitario a Ucrania y facilitar la llegada de refugiados a España.", 
            "Contamos con transportes para el envío de material humanitario a Ucrania y facilitar la llegada de refugiados a España.", 
            "Contamos con transportes para el envío de material humanitario a Ucrania y facilitar la llegada de refugiados a España.", 
            "Comptem amb persones traductores d’ucraïnès i rus."
            );
        this.IconBoxTextConceptThree = new Concept(
            "Disponemos de traductores con dominio y amplia experiencia nativos con el idioma ucraniano y ruso respectivamente.", 
            "Disponemos de traductores con dominio y amplia experiencia nativos con el idioma ucraniano y ruso respectivamente.", 
            "Disponemos de traductores con dominio y amplia experiencia nativos con el idioma ucraniano y ruso respectivamente.", 
            "Disponemos de traductores con dominio y amplia experiencia nativos con el idioma ucraniano y ruso respectivamente.", 
            "Tenim traductors d'Ucranià y Rus a la seva disposició."
            );
        this.IconBoxTextConceptFour = new Concept(
            "Horario de la oficina: c/ Flandes 6, de 10 a 14 y de 17 a 20 Horario de local: C/ Meridional  22 de 16 a 19", 
            "Horario de la oficina: c/ Flandes 6, de 10 a 14 y de 17 a 20 Horario de local: C/ Meridional  22 de 16 a 19", 
            "Horario de la oficina: c/ Flandes 6, de 10 a 14 y de 17 a 20 Horario de local: C/ Meridional  22 de 16 a 19", 
            "Horario de la oficina: c/ Flandes 6, de 10 a 14 y de 17 a 20 Horario de local: C/ Meridional  22 de 16 a 19", 
            "Adreça i horari de l’oficina: Flandes, 6, de 10 a 14h y de 17 a 20h. Aderça i horari del local: Meridional,  22, de 16 a 19h"
            );
    }
}
class AboutTranslations
{
    AboutTitleConcept;
    AboutTextConcept;
    AboutListElementConcept;
    AboutButtonTextConcept;
    GetCurrentLanguageAction;

    get AboutTitle()
    {
        return this.AboutTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutTextOne()
    {
        return this.AboutTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutTextTwo()
    {
        return this.AboutTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutTextThree()
    {
        return this.AboutTextConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutListTextOne()
    {
        return this.AboutListElementConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutListTextTwo()
    {
        return this.AboutListElementConcept.Translate(this.GetCurrentLanguageAction());
    }
    get AboutListTextThree()
    {
        return this.AboutListElementConcept.Translate(this.GetCurrentLanguageAction());
    } 
    get ButtonText()
    {
        return this.AboutButtonTextConcept.Translate(this.GetCurrentLanguageAction());
    }

    constructor(getCurrentLanguage)
    {
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.AboutTitleConcept = new Concept("SOBRE NOSOTROS", "ABOUT US", "про нас", "о нас", "Home");
        this.AboutTextConcept = new Concept(
            "Objetivo: Salvar vidas y minimizar el sufrimiento de las personas que como consecuencia de la guerra lo han perdido todo.",
            "Objetivo: Salvar vidas y minimizar el sufrimiento de las personas que como consecuencia de la guerra lo han perdido todo.",
            "Objetivo: Salvar vidas y minimizar el sufrimiento de las personas que como consecuencia de la guerra lo han perdido todo.",
            "Objetivo: Salvar vidas y minimizar el sufrimiento de las personas que como consecuencia de la guerra lo han perdido todo.",
            "Objetivo: Salvar vidas y minimizar el sufrimiento de las personas que como consecuencia de la guerra lo han perdido todo."
            );
        this.AboutListElementConcept = new Concept(this.LoremIpsumList,this.LoremIpsumList,this.LoremIpsumList,this.LoremIpsumList,this.LoremIpsumList,); 
        this.AboutButtonTextConcept = new Concept("Saber más", "Learn more", "Дізнайтеся більше", "Узнать больше", "Més informació");
    }
}
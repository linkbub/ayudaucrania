class CarouselTranslations
{
    CarouselTitleConcept;
    CarouselTextConcept;
    GetCurrentLanguageAction;
    LoremIpsum = "Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.";

    get CarouselTitle()
    {
        return this.CarouselTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextOne()
    {
        return this.CarouselTextConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextTwo()
    {
        return this.CarouselTextConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextThree()
    {
        return this.CarouselTextConceptThree.Translate(this.GetCurrentLanguageAction());
    }
    get CarouselTextFour()
    {
        return this.CarouselTextConceptFour.Translate(this.GetCurrentLanguageAction());
    }

    get CarouselButtonText()
    {
        return this.CarouselButtonTextConcept.Translate(this.GetCurrentLanguageAction());
    }

    constructor(getCurrentLanguage)
    {

        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.CarouselTitleConcept = new Concept(
            "Bienvenidos a PROYECTO SALVAVIDAS", 
            "Bienvenidos a PROYECTO SALVAVIDAS", 
            "Bienvenidos a PROYECTO SALVAVIDAS", 
            "Bienvenidos a PROYECTO SALVAVIDAS", 
            "Benvinguts a PROJECTE SALVAVIDES"
        );
        this.CarouselTextConceptOne = new Concept(
            "Organizamos viajes con material humanitario a Polonia y volvemos con personas que huyen de la situación que están viviendo", 
            "Organizamos viajes con material humanitario a Polonia y volvemos con personas que huyen de la situación que están viviendo", 
            "Organizamos viajes con material humanitario a Polonia y volvemos con personas que huyen de la situación que están viviendo",
            "Organizamos viajes con material humanitario a Polonia y volvemos con personas que huyen de la situación que están viviendo", 
            "Organitzem viatges amb material humanitari a Pol·lonia per tornar amb les persones que fugin de la situació que s'hi dona."
            );
        /* this.CarouselTitleConceptTwo = new Concept("Bienvenido a", "Wellcome", "ласкаво просимо до", "Добро пожаловать на", "Benvigut a"); */
        this.CarouselTextConceptTwo = new Concept(
            "Trabajamos diariamente para conseguir una igualdad de derechos", 
            "Trabajamos diariamente para conseguir una igualdad de derechos", 
            "Trabajamos diariamente para conseguir una igualdad de derechos", 
            "Trabajamos diariamente para conseguir una igualdad de derechos", 
            "Treballem diàriament per aconseguir la igualtat de drets"
            );
        /* this.CarouselTitleConceptThree = new Concept("Bienvenido a", "Wellcome", "ласкаво просимо до", "Добро пожаловать на", "Benvigut a"); */
        this.CarouselTextConceptThree = new Concept(
            "Trabajamos para conseguir un mundo de paz, amor y prosperidad para todas las personas.", 
            "Trabajamos para conseguir un mundo de paz, amor y prosperidad para todas las personas.", 
            "Trabajamos para conseguir un mundo de paz, amor y prosperidad para todas las personas.", 
            "Trabajamos para conseguir un mundo de paz, amor y prosperidad para todas las personas.", 
            "Volem aconseguir un món de pau, amor i prosperitat per a totes les persones."
            );

        this.CarouselTextConceptFour = new Concept(
            "Mantenemos informada a la comunidad de refugiados sobre las ayudas gubernamentales y de cualquier noticia que sean de su interés.", 
            "Mantenemos informada a la comunidad de refugiados sobre las ayudas gubernamentales y de cualquier noticia que sean de su interés.", 
            "Mantenemos informada a la comunidad de refugiados sobre las ayudas gubernamentales y de cualquier noticia que sean de su interés.", 
            "Mantenemos informada a la comunidad de refugiados sobre las ayudas gubernamentales y de cualquier noticia que sean de su interés.", 
            "Mantenim informada a la comunitat de refugiats sobre l'ajut gubernamental y de qualsevol notícia que sigui del seu interès."
            );
        this.CarouselButtonTextConcept = new Concept("Leer más", "Read more", "Детальніше", "Подробнее", "Llegir més");
    }
}
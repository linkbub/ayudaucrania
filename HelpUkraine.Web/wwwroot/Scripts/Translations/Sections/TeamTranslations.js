class TeamTranslations
{
    GetCurrentLanguageAction;
    TeamTitleConcept;
    TeamTextConcept;

    get TeamTitle(){
        return this.TeamTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get TeamText(){
        return this.TeamTextConcept.Translate(this.GetCurrentLanguageAction());
    }


    constructor(getCurrentLanguage)
    {
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.TeamTitleConcept = new Concept(
            "Equipo",
            "Equipo",
            "Equipo",
            "Equipo",
            "Equip"
        );
        this.TeamTextConcept = new Concept(
            "Nuestro equipo de trabajo consta de voluntarios que dedican su tiempo a la ayuda para potenciar las acciones y actividades. Cualquier persona que quiera dedicar su tiempo a mejorar nuestra sociedad es bienvenida. DONES AMF es una asociación para mujeres no de mujeres motivo por el cual tenemos un alto índice de asociados.",
            "Nuestro equipo de trabajo consta de voluntarios que dedican su tiempo a la ayuda para potenciar las acciones y actividades. Cualquier persona que quiera dedicar su tiempo a mejorar nuestra sociedad es bienvenida. DONES AMF es una asociación para mujeres no de mujeres motivo por el cual tenemos un alto índice de asociados.",
            "Nuestro equipo de trabajo consta de voluntarios que dedican su tiempo a la ayuda para potenciar las acciones y actividades. Cualquier persona que quiera dedicar su tiempo a mejorar nuestra sociedad es bienvenida. DONES AMF es una asociación para mujeres no de mujeres motivo por el cual tenemos un alto índice de asociados.","Nuestro equipo de trabajo consta de voluntarios que dedican su tiempo a la ayuda para potenciar las acciones y actividades. Cualquier persona que quiera dedicar su tiempo a mejorar nuestra sociedad es bienvenida. DONES AMF es una asociación para mujeres no de mujeres motivo por el cual tenemos un alto índice de asociados.",
            "El nostre equip de treball consta de persones voluntàries que dediquen el seu temps a potenciar accions i activitats.Qualsevol persona que vulgui aportar el seu temps a millorar la nostra societat és benvinguda.DONES AMF és una associació per a dones, no de dones, per tant tenim un alt índex de persones associades."
        )
    }
}
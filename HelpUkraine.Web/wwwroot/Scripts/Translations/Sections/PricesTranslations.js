class PricesTranslations
{
    GetCurrentLanguageAction;
    PriceTitleConcept;
    PriceCardTitleConceptOne;
    PriceCardTitleConceptTwo;
    PriceCardInfoConceptOne;
    PriceCardInfoConceptTwo;

    get PriceTitle(){
        return this.PriceTitleConcept.Translate(this.GetCurrentLanguageAction());
    }
    get PriceCardTitleOne(){
        return this.PriceCardTitleConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get PriceCardInfoOne(){
        return this.PriceCardInfoConceptOne.Translate(this.GetCurrentLanguageAction());
    }
    get PriceCardTitleTwo(){
        return this.PriceCardTitleConceptTwo.Translate(this.GetCurrentLanguageAction());
    }
    get PriceCardInfoTwo(){
        return this.PriceCardInfoConceptTwo.Translate(this.GetCurrentLanguageAction());
    }

    constructor(getCurrentLanguage){
        this.GetCurrentLanguageAction = getCurrentLanguage;
        this.PriceTitleConcept = new Concept(
            "CUOTA ASOCIADOS",
            "CUOTA ASOCIADOS",
            "CUOTA ASOCIADOS",
            "CUOTA ASOCIADOS",
            "QUOTA PERSONES ASSOCIADES"
        );
        this.PriceCardTitleConceptOne = new Concept(
            "Quota social",
            "Cuota social",
            "Cuota social",
            "Cuota social",
            "Quota social"
        );
        this.PriceCardInfoConceptOne = new Concept(
            "Menores de 18 años y jubilados",
            "Menores de 18 años y jubilados",
            "Menores de 18 años y jubilados",
            "Menores de 18 años y jubilados",
            "Menors de 18 anys i persones jubilades"
        );
        this.PriceCardTitleConceptTwo = new Concept(
            "Cuota asociada",
            "Cuota asociada",
            "Cuota asociada",
            "Cuota asociada",
            "Quota associada",
        );
        this.PriceCardInfoConceptTwo = new Concept(
            "15€ semestral",
            "15€ semestral",
            "15€ semestral",
            "15€ semestral",
            "15€ semestrals",
        );
    }
}